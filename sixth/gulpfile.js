livereload = require('electron-livereload')
 
electron = livereload.server()
 
module.exports = function (grunt) {
 
  grunt.initConfig({
 
    watch:  {
      options:  {
        nospawn: true // !IMPORTANT!
      },
      client:  {
        files: ['src/client/**/*.coffee'], tasks: ['coffee', 'reload-electron']
      },
      server: {
        files: ['src/server/**/*.coffee'], tasks: ['coffee', 'restart-electron']
      }
    }
      
  })
    grunt.registerTask('start', function (env)  {
      electron.start()
      grunt.task.run('watch')
    })
      
    grunt.registerTask('restart-electron', function () {
      electron.restart()
    })
    
    grunt.registerTask('reload-electron', function () {
      electron.reload()
    })
    
}
