app     = require('app')
window  = require('browser-window')
 
livereload = require('electron-livereload')
 
app.on('ready', function () {
  win = new window({
    title: 'My App'
    'min-width': 520
    'min-height': 520
    frame: false
    resizable: true
    icon: 'assets/images/icon.png'
    transparent: true
    center: true
  })

  win.loadUrl('file://' + path.join __dirname, 'index.html')
  
  livereload.client(win)
})
