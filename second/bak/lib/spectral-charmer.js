var main = require('main-loop')
, Kefir = require('kefir')
, appEl = document.getElementById('app')

module.exports = {

//  views: {
//    Spectogram: require('./views/Spectrogram.js'),
//    BarGraph: require('./views/BarGraph.js'),
//    Words: require('./views/Words.js'),
//    Histogram: require('./views/Histogram.js'),
//  },
//
//  patches: {
//    Faucet: require('./patches/Faucet.js'),
//    FFT: require('./patches/FFT.js'),
//    SpikeDetector: require('./patches/SpikeDetector.js'),
//  },

  bootstrap: function (app) {

    // remove old listeners from the websocket
    socket.removeAllListeners()

    // remove everything from our container
    appEl.innerHTML = ''
    
    // setup a function by which stuff in render can draw on the dom
    function draw (track, view, docstring) {
    
      // add a div to the page
      var parent = document.createElement('div')
      var doc    = document.createTextNode(docstring)
      parent.appendChild(doc)
      appEl.appendChild(parent)
    
      // execute the view module to return the view's draw fn
      // (this lets us keep state in the view module)
      var viewDrawFn = view()
    
      var loop = main([], viewDrawFn, require('virtual-dom'))
    
      // add loop to the div
      parent.appendChild(loop.target)
    
      // set each value in the track's output stream 
      // to trigger a `loop.update`, which in turn triggers viewDrawFn
      track.onValue(loop.update)
    
    }

    // pass the stream and the draw fn into the app bootstrap 
    app(socket, draw)

  }

}
