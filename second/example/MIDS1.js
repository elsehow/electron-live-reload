'use strict';

var Faucet = require('../lib/patches/Faucet.js')

module.exports = function render (socket, draw) {

  var ns   = Faucet(socket, 'mids-mindwave-playback')

  function attn (r) {
    return r.raw_values
  }

  ns.map(attn).log()

}
